import java.util.*;

/**
 *	Interface that all compression suites must implement. That is they must be
 *	able to compress a file and also reverse/decompress that process.
 * 
 *	@author Brian Lavallee
 *	@since 5 November 2015
 *  @author Owen Atrachan
 *  @since December 1, 2016
 */
public class HuffProcessor {

	public static final int BITS_PER_WORD = 8;
	public static final int BITS_PER_INT = 32;
	public static final int ALPH_SIZE = (1 << BITS_PER_WORD); // or 256
	public static final int PSEUDO_EOF = ALPH_SIZE;
	public static final int HUFF_NUMBER = 0xface8200;
	public static final int HUFF_TREE  = HUFF_NUMBER | 1;
	public static final int HUFF_COUNTS = HUFF_NUMBER | 2;

	public enum Header{TREE_HEADER, COUNT_HEADER};
	public Header myHeader = Header.TREE_HEADER;
	
	/**
	 * Compresses a file. Process must be reversible and loss-less.
	 *
	 * @param in
	 *            Buffered bit stream of the file to be compressed.
	 * @param out
	 *            Buffered bit stream writing to the output file.
	 */
	public void compress(BitInputStream in, BitOutputStream out){
	    /*while (true){
	        int val = in.readBits(BITS_PER_WORD);
	        if (val == -1) break;
	        
	        out.writeBits(BITS_PER_WORD, val);
	    }
	    */
		int[] counts = readForCounts(in);
		HuffNode root = makeTreeFromCounts(counts);
		HashMap<Integer, String> codings = new HashMap<Integer, String>();
		makeCodingsFromTree(root, "", codings);
		out.writeBits(BITS_PER_INT, HUFF_TREE);
		writeHeader(out, root);
		writeCompressedBits(in, out, codings);
	}
	
	private int[] readForCounts(BitInputStream in) {
		int[] counts = new int[256];
		int val = 0;
		while(true) {
			val = in.readBits(BITS_PER_WORD);
			if(val == -1) {
				break;
			}
			counts[val] = counts[val] + 1;
		}
		return counts;
	}
	
	private HuffNode makeTreeFromCounts(int[] counts) {
		PriorityQueue<HuffNode> pq = new PriorityQueue<HuffNode>();
		for(int i = 0; i < counts.length; i++) {
			if(counts[i] > 0) {
				pq.add(new HuffNode(i, counts[i]));
			}
		}
		pq.add(new HuffNode(PSEUDO_EOF, 1));
		while(pq.size() > 1) {
			HuffNode left = pq.remove();
			HuffNode right = pq.remove();
			HuffNode t = new HuffNode(-1, left.weight() + right.weight(), left, right);
			pq.add(t);
		}
		HuffNode root = pq.remove();
		return root;
	}
	
	private void makeCodingsFromTree(HuffNode root, String progress, HashMap<Integer, String> codings) {
		HuffNode current = root;
		if(current.value() == -1) {
				String progressLeft = progress + "0";
				String progressRight = progress + "1";
				makeCodingsFromTree(current.left(), progressLeft, codings);
				makeCodingsFromTree(current.right(), progressRight, codings);
		}
		else {
			Integer value = current.value();
			codings.put(value, progress);
		}
	}
	
	private void writeHeader(BitOutputStream out, HuffNode root) {
		HuffNode current = root;
		if(current.value() == -1) {
			out.writeBits(1, 0);
			writeHeader(out, current.left());
			writeHeader(out, current.right());
		}
		else {
			out.writeBits(1, 1);
			out.writeBits(BITS_PER_WORD + 1, current.value());
		}
	}
	
	private void writeCompressedBits(BitInputStream in, BitOutputStream out, HashMap<Integer, String> codings) {
		in.reset();
		while(true) {
			int val = in.readBits(BITS_PER_WORD);
			if(val != -1) {
				String compressed = codings.get(val);
				int writeable = Integer.parseInt(compressed, 2);
				out.writeBits(compressed.length(), writeable);
			}
			if(val == -1) {
				String compressed = codings.get(PSEUDO_EOF);
				int writeable = Integer.parseInt(compressed, 2);
				out.writeBits(compressed.length(), writeable);
				break;
			}
		}		
	}
	

	/**
	 * Decompresses a file. Output file must be identical bit-by-bit to the
	 * original.
	 *
	 * @param in
	 *            Buffered bit stream of the file to be decompressed.
	 * @param out
	 *            Buffered bit stream writing to the output file.
	 */
	public void decompress(BitInputStream in, BitOutputStream out){
	    /*while (true){
            int val = in.readBits(BITS_PER_WORD);
            if (val == -1) break;
            
            out.writeBits(BITS_PER_WORD, val);
        }
	    */
		int val = 0;
		val = in.readBits(BITS_PER_INT);
		System.out.println(val);
		System.out.println(HUFF_TREE);
		if(val != HUFF_TREE) {
			throw new HuffException("This is not a Huffman type file.");
		}
		HuffNode tree = readTreeHeader(in);
		readCompressedBits(in, out, tree);
	}
	
	private HuffNode readTreeHeader(BitInputStream in) {
		int current = in.readBits(1);
		HuffNode node = null;
		if(current == 1) {
			int val = in.readBits(BITS_PER_WORD + 1);
			node = new HuffNode(val, 0);
		}
		if(current == 0) {
			HuffNode left = readTreeHeader(in);
			HuffNode right = readTreeHeader(in);
			node = new HuffNode(0, 0, left, right);
		}
		return node;
	}
	
	private void readCompressedBits(BitInputStream in, BitOutputStream out, HuffNode head) {
		HuffNode progress = head;
		while(true) {
			int current = in.readBits(1);
			int val = 0;
			if(current == 0) {
				progress = progress.left();
				if(progress.value() != 0) {
					val = progress.value();
					if(val == PSEUDO_EOF) {
						break;
					}
					out.writeBits(BITS_PER_WORD, val);
					progress = head;
				}
			}
			if(current == 1) {
				progress = progress.right();
				if(progress.value() != 0) {
					val = progress.value();
					if(val == PSEUDO_EOF) {
						break;
					}
					out.writeBits(BITS_PER_WORD, val);
					progress = head;
				}
			}
		}
	}
	
	public void setHeader(Header header) {
        myHeader = header;
        System.out.println("header set to "+myHeader);
    }
}